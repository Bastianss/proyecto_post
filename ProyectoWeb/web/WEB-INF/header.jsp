<%@page import="mvc.modelo.entidades.Users"%>
<%
    Users usuario = null;
    if (session.getAttribute("datos") == null) {
%>
<jsp:forward page="index.jsp"/>
<%    } else {
        usuario = (Users) session.getAttribute("datos");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            
            #imgid{
                border-radius: 50%;
                border-style: solid;
                border-width: thin;
            }

        </style>
          <link rel="shortcut icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=   1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://chrix.000webhostapp.com/mypost/estilo.css">
        <link href="https://chrix.000webhostapp.com/mypost/editor.css" rel="stylesheet">
        <script src="https://chrix.000webhostapp.com/mypost/editor.js"></script>
        <script src="https://chrix.000webhostapp.com/mypost/script.js"></script>
        <meta charset="utf-8">
        <title>MyPost's</title>
    </head>
    <body>
        <nav style="position: fixed;z-index:999;width:100%;background-color:#FFe800;" class="navbar navbar-expand-lg navbar-light bg-warning">
            <a class="navbar-brand" style="color:white;font-family: 'Pacifico';  text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;" href="/MyPost    ">MyPost's</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/MyPost">Inicio <span class="sr-only"></span></a>
                    </li>
                    <li>
                        <a class="nav-link" href="hastag.do">#Hashtags</a>
                    </li>
                </ul>
                <form method="get" action="search.do?=" class="form-inline my-2 my-lg-0">
                    <input style="width:100%;background-color:#f9f9f9;" class="dropdown-item form-control mr-sm-2" type="text" placeholder="Buscar..." name="q">
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li style="margin-right:80px;margin-left:20px" class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <%=usuario.getUserName()%>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="perfil.jsp">Perfil</a>
                            <a class="dropdown-item"  data-toggle="modal" data-target="#ajustes" href="#">Ajustes</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="logout.do">Cerrar sesi�n</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div style="padding-top: 70px;" class="row">
            <div style="background-color:white;padding:20px;width: 100%;" class="jumbotron">

                <div class="modal fade" id="ajustes">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Ajustes</h4> <button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                            <div class="modal-body">
                                <form method="post" action="ajustes.do">
                                    <div class="row">
                                        <div class="col-sm">
                                            <img src="<%=usuario.getUserPicture()%>" id="imgid" width="140px" height="140px">
                                            <label style="margin-top: 10px" class="fileContainer">
                                                Seleccionar archivo
                                                <input type="file"/>
                                            </label>   
                                        </div>
                                        <div style="margin-top: auto;margin-bottom: auto" class="col-sm">
                                            <h5>Descripcion:</h5>
                                            <div class="form-group">              <input type="text" maxlength="45" class="form-control" name="description" value="<%=usuario.getUserDescription()%>"><br>
                                            </div>
                                        </div>     </div>
                                    <h5>Cambiar clave:</h5>
                                    <hr>
                                    <div class="form-group"> <label for="pwd">Clave:</label> <input type="password" class="form-control" id="pwd" placeholder="Introduce tu clave" name="clave"> </div>
                                    <div class="form-group"> <label for="pwd">Confirmar clave:</label> <input type="password" class="form-control" id="pwd" placeholder="Introduce tu clave" name="clave"> </div>

                                    <button type="submit" class="btn btn-block btn-warning"><h3>Guardar </h3></button> </form>
                            </div>
                            <div class="modal-footer"> </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="nPost">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div style="padding-bottom:100px" class="modal-body">
                                <h2 align="center">A�adir post <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">�</span></button></h2>
                                <input style="border-radius:0px;margin-left:auto;margin-bottom:15px;width:100%" type="text" class="form-control" id="hastags" placeholder="Hastag..." name="hastags">
                                <div style="background-color:white;margin:auto; height:300px">
                                    <div style="background-color:#ededed;" id="editor">
                                    </div>
                                    <button onclick="post(quill.container.firstChild.innerHTML)" type="button" style="margin-top:10px" class="btn btn-warning btn-block" data-dismiss="modal">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        var quill = new Quill('#editor', {
                            theme: 'snow'
                        });
                    </script>
                </div>


