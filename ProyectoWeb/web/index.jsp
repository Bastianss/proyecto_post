<%
    if (session.getAttribute("datos") != null) {
        response.sendRedirect("inicio.do");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=0.9">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://chrix.000webhostapp.com/mypost/estilo.css">
        <script src="https://chrix.000webhostapp.com/mypost/script.js"></script>

        <title>MyPost's</title>
    </head>
    <div class="titulo" style="color: white;font-family: 'Pacifico';text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;" align="center"><a href="index.jsp" class="link">MyPost's</a></div>

    <body style="background-size: cover;">
        <div style="padding: 5px 15px 0;" class="container">

            <div class="row">
                <div class="col-sm-7 c1" style="background-color:white;">
                    <div style="padding: 30px 15px 0;" class="container">
                        <%if (request.getAttribute("error") != null) {
                                out.print("<div class='alert alert-danger'>" + request.getAttribute("error") + "</div>");
                            } else {
                                out.print("<div style='opacity: .0;' class='alert'></div>");
                            }%>
                        <form method="post"action="login.do">
                            <div class="form-group"> <label for="username">Nombre</label> <input type="text" class="form-control" placeholder="Introduce tu usuario" name="usuario"> </div>
                            <div class="form-group"> <label for="pwd">Clave:</label> <input type="password" class="form-control" placeholder="Introduce tu clave" name="clave"> </div>
                            <button type="submit" style="margin-top: 15px;height:50px" class="btn btn-block btn-warning"><h3>Entrar</h3></button> </form><br>
                    </div>
                </div>
                <div class="col-sm-5 c2" style="background-color:LightGray;">
                    <div style="    padding: 35% 0;" class="container">
                        <h3 align="center">¿Aún no tienes cuenta?</h3>
                        <p align="center">Regístrate ahora y empieza a disfrutar de una nueva forma de comunicarte.</p>
                        <button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#Registro"><h5> Registro </h5></button>
                        <div class="modal fade" id="Registro">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Registro</h4> <button type="button" class="close" data-dismiss="modal">&times;</button> </div>
                                    <div class="modal-body">
                                        <form method="post" action="registro.do">
                                            <div class="form-group"> <label for="name">Nombre:</label> <input type="text" class="form-control" id="name" placeholder="Introduce tu nombre" name="usuario"> </div>
                                            <div class="form-group"> <label for="email">Email:</label> <input type="email" class="form-control" id="email" placeholder="Introduce tu email" name="email"> </div>
                                            <span id='message'></span>
                                            <div class="form-group"> <label for="pwd">Clave:</label> <input type="password" class="form-control" id="pwd" placeholder="Introduce tu clave" name="clave" onkeyup='check();' /> </div>
                                            <div class="form-group"> <label for="cpwd">Confirmar clave:</label> <input type="password" id="cpwd" class="form-control" placeholder="Introduce tu clave" name="cclave" onkeyup='check();' /> </div>

                                            <button disabled="true" type="submit" id="reg" class="btn btn-block btn-warning"><h3>Registrar  </h3></button> </form>
                                    </div>
                                    <div class="modal-footer"> </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <br>
        <footer class="footer">
            <div class="container">
                <p align="center" class="h3">Copyrigth FundacionEsplai 2018</p>
            </div>
        </footer>
    </body>

</html>



