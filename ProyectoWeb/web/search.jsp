<%@page import="mvc.modelo.entidades.Users"%>
<%@page import="mvc.modelo.entidades.Hastag"%>
<%@page import="java.util.List"%>
<%@page import="mvc.modelo.entidades.Post"%>
<jsp:include page="WEB-INF/header.jsp"/>

<%Users usuario = (Users) session.getAttribute("datos");%>

<h1 align="center">Resultados de la busqueda</h1>
<hr />
<%
    boolean error;
    try {
        error = (Boolean) request.getAttribute("error");
    } catch (Exception e) {
        error = false;
    }
    if (error) {
        out.print("<div class='alert alert-danger'><h4 align='center'>No se obtuvieron resultados</h4></div>");
    } else {
        Hastag qh = new Hastag(request.getParameter("q"));
        if (!usuario.getHastagList().contains(qh)) {
            session.setAttribute("addHastag", qh);
            out.print("<a href='hastag.do?a=" + qh.getHastagName() + "' class='btn btn-block btn-success'>A�adir " + qh.getHastagName() + " a mis Hastags</a>");
        } else {
            out.print("<a href='hastag.do?d=" + qh.getHastagName() + "' class='btn btn-block btn-danger'>Eliminar " + qh.getHastagName() + " de mis Hastags</a>");
        }
        Hastag hs = (Hastag) session.getAttribute("resultado");
        List<Post> listaPost = hs.getPostList();

        for (Post p : listaPost) {
            out.print("<div class='col-md-12'>");
            out.print("<p>" + p.getDescription() + "</p>");
            out.print("<a href='view.do?u=" + p.getIdUser().getUserName() + "'>@" + p.getIdUser().getUserName() + "</a><span class='badge'>" + p.getDateWrite() + "</span><div class='pull-right'>");
            List<Hastag> lhastags = p.getHastagList();
            for (Hastag h : lhastags) {
                out.print("<a href='search.do?q=" + h.getHastagName() + "' class='badge badge-warning'>" + h.getHastagName() + "</a> ");
            }
            out.print("</div></div><hr>");
        }
    }
%>                     
<jsp:include page="WEB-INF/foooter.jsp"/>
