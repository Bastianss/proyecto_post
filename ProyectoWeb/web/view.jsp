<%@page import="mvc.modelo.entidades.Hastag"%>
<%@page import="mvc.modelo.entidades.Post"%>
<%@page import="java.util.List"%>
<%@page import="mvc.modelo.entidades.Users"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%Users usuarioP = (Users) session.getAttribute("perfil");%>
<jsp:include page="WEB-INF/header.jsp"/>

<div style="border-radius: 10%;background-color: #ededed43"class="row">
    <div class="center"><div style="float: right" class="col-sm">
            <img width="200px"height="200px" src="<%=usuarioP.getUserPicture()%>" style="border-radius: 50%"/><br>
        </div></div>
    <div style="padding: 45px;padding-bottom: 15px;padding-top: 0px"class="col-sm">
        <h1 style="color:#ffc107; text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;"align="center"><%=usuarioP.getUserName()%></h1><br>
        <h3 style="font-weight: bold" align="center"><%=usuarioP.getUserDescription()%></h3>
        <br>
    </div>
</div>
<hr>
<%
    List<Post> listaPost = usuarioP.getPostList();
    for (Post p : listaPost) {
        out.print("<div class='col-md-12'>");
        out.print("<p>" + p.getDescription() + "</p>");
        out.print("<a href='view.do?u=" + usuarioP.getUserName() + "'>@" + usuarioP.getUserName() + "</a><span class='badge'>" + p.getDateWrite() + "</span><div class='pull-right'>");
        List<Hastag> lhastags = p.getHastagList();
        for (Hastag h : lhastags) {
            out.print("<a href='search.do?q=" + h.getHastagName() + "' class='badge badge-warning'>" + h.getHastagName() + "</a> ");
        }
        out.print("</div></div><hr>");
    }
%>
<jsp:include page="WEB-INF/foooter.jsp"/>
