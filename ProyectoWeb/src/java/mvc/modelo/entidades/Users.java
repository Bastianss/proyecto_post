/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ChRix
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    //, @NamedQuery(name = "Users.addHastag", query = "INSERT INTO hatags_users (hastag_name, id_user) VALUES (:hastag, :usuario)")
    , @NamedQuery(name = "Users.findByIdUsers", query = "SELECT u FROM Users u WHERE u.idUsers = :idUsers")
    , @NamedQuery(name = "Users.findByUserName", query = "SELECT u FROM Users u WHERE u.userName = :userName")
    , @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")
    , @NamedQuery(name = "Users.findByDateRegister", query = "SELECT u FROM Users u WHERE u.dateRegister = :dateRegister")
    , @NamedQuery(name = "Users.findByUserDescription", query = "SELECT u FROM Users u WHERE u.userDescription = :userDescription")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_users")
    private Integer idUsers;
    @Basic(optional = false)
    @Column(name = "user_name")
    private String userName;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "date_register")
    private String dateRegister;
    @Basic(optional = false)
    @Column(name = "user_description")
    private String userDescription;
    @Basic(optional = false)
    @Column(name = "user_picture")
    private String userPicture;
    @JoinTable(name = "followers", joinColumns = {
        @JoinColumn(name = "id_followers", referencedColumnName = "id_users")}, inverseJoinColumns = {
        @JoinColumn(name = "id_user", referencedColumnName = "id_users")})
    @ManyToMany
    private List<Users> usersList;
    @ManyToMany(mappedBy = "usersList")
    private List<Users> usersList1;
    @ManyToMany(mappedBy = "usersList")
    private List<Hastag> hastagList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUser")
    @OrderBy("idPost DESC")
    private List<Post> postList;

    public Users() {
    }

    public Users(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public String getUserPicture() {
        if (userPicture==null) {
            userPicture="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg";
        }
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public Users(String name, String email, String password, String fecha, String description) {
        this.userName = name;
        this.email = email;
        this.password = password;
        this.dateRegister = fecha;
        this.userDescription = "Hola,soy nuevo en MyPost";

    }

    public Users(Integer idUsers, String userName, String email, String password, String dateRegister, String userDescription) {
        this.idUsers = idUsers;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.dateRegister = dateRegister;
        this.userDescription = userDescription;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(String dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    @XmlTransient
    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @XmlTransient
    public List<Users> getUsersList1() {
        return usersList1;
    }

    public void setUsersList1(List<Users> usersList1) {
        this.usersList1 = usersList1;
    }

    @XmlTransient
    public List<Hastag> getHastagList() {
        return hastagList;
    }

    public void setHastagList(List<Hastag> hastagList) {
        this.hastagList = hastagList;
    }

    @XmlTransient
    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsers != null ? idUsers.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.idUsers == null && other.idUsers != null) || (this.idUsers != null && !this.idUsers.equals(other.idUsers))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.Users[ idUsers=" + idUsers + " ]";
    }

}
