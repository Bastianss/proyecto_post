/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ChRix
 */
@Entity
@Table(name = "hastag")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hastag.findAll", query = "SELECT h FROM Hastag h")
    , @NamedQuery(name = "Hastag.findByHastagName", query = "SELECT h FROM Hastag h WHERE h.hastagName = :hastagName")})
public class Hastag implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "hastag_name")
    private String hastagName;
    @JoinTable(name = "hatags_users", joinColumns = {
        @JoinColumn(name = "hastag_name", referencedColumnName = "hastag_name")}, inverseJoinColumns = {
        @JoinColumn(name = "id_user", referencedColumnName = "id_users")})
    @ManyToMany
    private List<Users> usersList;
    @JoinTable(name = "hastag_post", joinColumns = {
        @JoinColumn(name = "hastag_name", referencedColumnName = "hastag_name")}, inverseJoinColumns = {
        @JoinColumn(name = "id_post", referencedColumnName = "id_post")})

    @ManyToMany
    @OrderBy("idPost DESC")

    private List<Post> postList;

    public Hastag() {
    }

    public Hastag(String hastagName) {
        this.hastagName = hastagName;
    }

    public String getHastagName() {
        return hastagName;
    }

    public void setHastagName(String hastagName) {
        this.hastagName = hastagName;
    }

    @XmlTransient
    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @XmlTransient
    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hastagName != null ? hastagName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hastag)) {
            return false;
        }
        Hastag other = (Hastag) object;
        if ((this.hastagName == null && other.hastagName != null) || (this.hastagName != null && !this.hastagName.equals(other.hastagName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.Hastag[ hastagName=" + hastagName + " ]";
    }

}
