/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mvc.modelo.entidades.Users;
import mvc.modelo.entidades.Hastag;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mvc.controlador.entidades.exceptions.NonexistentEntityException;
import mvc.controlador.entidades.exceptions.PreexistingEntityException;
import mvc.modelo.entidades.Post;

/**
 *
 * @author ChRix
 */
public class PostJpaController implements Serializable {

    public PostJpaController() {
        this.emf = Persistence.createEntityManagerFactory("MyPostPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Post post) throws PreexistingEntityException, Exception {
        if (post.getHastagList() == null) {
            post.setHastagList(new ArrayList<Hastag>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users idUser = post.getIdUser();
            if (idUser != null) {
                idUser = em.getReference(idUser.getClass(), idUser.getIdUsers());
                post.setIdUser(idUser);
            }
            List<Hastag> attachedHastagList = new ArrayList<Hastag>();
            for (Hastag hastagListHastagToAttach : post.getHastagList()) {
                hastagListHastagToAttach = em.getReference(hastagListHastagToAttach.getClass(), hastagListHastagToAttach.getHastagName());
                attachedHastagList.add(hastagListHastagToAttach);
            }
            post.setHastagList(attachedHastagList);
            em.persist(post);
            if (idUser != null) {
                idUser.getPostList().add(post);
                idUser = em.merge(idUser);
            }
            for (Hastag hastagListHastag : post.getHastagList()) {
                hastagListHastag.getPostList().add(post);
                hastagListHastag = em.merge(hastagListHastag);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPost(post.getIdPost()) != null) {
                throw new PreexistingEntityException("Post " + post + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Post post) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Post persistentPost = em.find(Post.class, post.getIdPost());
            Users idUserOld = persistentPost.getIdUser();
            Users idUserNew = post.getIdUser();
            List<Hastag> hastagListOld = persistentPost.getHastagList();
            List<Hastag> hastagListNew = post.getHastagList();
            if (idUserNew != null) {
                idUserNew = em.getReference(idUserNew.getClass(), idUserNew.getIdUsers());
                post.setIdUser(idUserNew);
            }
            List<Hastag> attachedHastagListNew = new ArrayList<Hastag>();
            for (Hastag hastagListNewHastagToAttach : hastagListNew) {
                hastagListNewHastagToAttach = em.getReference(hastagListNewHastagToAttach.getClass(), hastagListNewHastagToAttach.getHastagName());
                attachedHastagListNew.add(hastagListNewHastagToAttach);
            }
            hastagListNew = attachedHastagListNew;
            post.setHastagList(hastagListNew);
            post = em.merge(post);
            if (idUserOld != null && !idUserOld.equals(idUserNew)) {
                idUserOld.getPostList().remove(post);
                idUserOld = em.merge(idUserOld);
            }
            if (idUserNew != null && !idUserNew.equals(idUserOld)) {
                idUserNew.getPostList().add(post);
                idUserNew = em.merge(idUserNew);
            }
            for (Hastag hastagListOldHastag : hastagListOld) {
                if (!hastagListNew.contains(hastagListOldHastag)) {
                    hastagListOldHastag.getPostList().remove(post);
                    hastagListOldHastag = em.merge(hastagListOldHastag);
                }
            }
            for (Hastag hastagListNewHastag : hastagListNew) {
                if (!hastagListOld.contains(hastagListNewHastag)) {
                    hastagListNewHastag.getPostList().add(post);
                    hastagListNewHastag = em.merge(hastagListNewHastag);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = post.getIdPost();
                if (findPost(id) == null) {
                    throw new NonexistentEntityException("The post with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Post post;
            try {
                post = em.getReference(Post.class, id);
                post.getIdPost();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The post with id " + id + " no longer exists.", enfe);
            }
            Users idUser = post.getIdUser();
            if (idUser != null) {
                idUser.getPostList().remove(post);
                idUser = em.merge(idUser);
            }
            List<Hastag> hastagList = post.getHastagList();
            for (Hastag hastagListHastag : hastagList) {
                hastagListHastag.getPostList().remove(post);
                hastagListHastag = em.merge(hastagListHastag);
            }
            em.remove(post);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Post> findPostEntities() {
        return findPostEntities(true, -1, -1);
    }

    public List<Post> findPostEntities(int maxResults, int firstResult) {
        return findPostEntities(false, maxResults, firstResult);
    }

    private List<Post> findPostEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Post.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Post findPost(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Post.class, id);
        } finally {
            em.close();
        }
    }

    public int getPostCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Post> rt = cq.from(Post.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
