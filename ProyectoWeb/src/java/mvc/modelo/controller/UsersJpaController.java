/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mvc.modelo.entidades.Users;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mvc.controlador.entidades.exceptions.IllegalOrphanException;
import mvc.controlador.entidades.exceptions.NonexistentEntityException;
import mvc.modelo.entidades.Hastag;
import mvc.modelo.entidades.Post;

/**
 *
 * @author ChRix
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController() {
        this.emf = Persistence.createEntityManagerFactory("MyPostPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) {
        if (users.getUsersList() == null) {
            users.setUsersList(new ArrayList<Users>());
        }
        if (users.getUsersList1() == null) {
            users.setUsersList1(new ArrayList<Users>());
        }
        if (users.getHastagList() == null) {
            users.setHastagList(new ArrayList<Hastag>());
        }
        if (users.getPostList() == null) {
            users.setPostList(new ArrayList<Post>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Users> attachedUsersList = new ArrayList<Users>();
            for (Users usersListUsersToAttach : users.getUsersList()) {
                usersListUsersToAttach = em.getReference(usersListUsersToAttach.getClass(), usersListUsersToAttach.getIdUsers());
                attachedUsersList.add(usersListUsersToAttach);
            }
            users.setUsersList(attachedUsersList);
            List<Users> attachedUsersList1 = new ArrayList<Users>();
            for (Users usersList1UsersToAttach : users.getUsersList1()) {
                usersList1UsersToAttach = em.getReference(usersList1UsersToAttach.getClass(), usersList1UsersToAttach.getIdUsers());
                attachedUsersList1.add(usersList1UsersToAttach);
            }
            users.setUsersList1(attachedUsersList1);
            List<Hastag> attachedHastagList = new ArrayList<Hastag>();
            for (Hastag hastagListHastagToAttach : users.getHastagList()) {
                hastagListHastagToAttach = em.getReference(hastagListHastagToAttach.getClass(), hastagListHastagToAttach.getHastagName());
                attachedHastagList.add(hastagListHastagToAttach);
            }
            users.setHastagList(attachedHastagList);
            List<Post> attachedPostList = new ArrayList<Post>();
            for (Post postListPostToAttach : users.getPostList()) {
                postListPostToAttach = em.getReference(postListPostToAttach.getClass(), postListPostToAttach.getIdPost());
                attachedPostList.add(postListPostToAttach);
            }
            users.setPostList(attachedPostList);
            em.persist(users);
            for (Users usersListUsers : users.getUsersList()) {
                usersListUsers.getUsersList().add(users);
                usersListUsers = em.merge(usersListUsers);
            }
            for (Users usersList1Users : users.getUsersList1()) {
                usersList1Users.getUsersList().add(users);
                usersList1Users = em.merge(usersList1Users);
            }
            for (Hastag hastagListHastag : users.getHastagList()) {
                hastagListHastag.getUsersList().add(users);
                hastagListHastag = em.merge(hastagListHastag);
            }
            for (Post postListPost : users.getPostList()) {
                Users oldIdUserOfPostListPost = postListPost.getIdUser();
                postListPost.setIdUser(users);
                postListPost = em.merge(postListPost);
                if (oldIdUserOfPostListPost != null) {
                    oldIdUserOfPostListPost.getPostList().remove(postListPost);
                    oldIdUserOfPostListPost = em.merge(oldIdUserOfPostListPost);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getIdUsers());
            List<Users> usersListOld = persistentUsers.getUsersList();
            List<Users> usersListNew = users.getUsersList();
            List<Users> usersList1Old = persistentUsers.getUsersList1();
            List<Users> usersList1New = users.getUsersList1();
            List<Hastag> hastagListOld = persistentUsers.getHastagList();
            List<Hastag> hastagListNew = users.getHastagList();
            List<Post> postListOld = persistentUsers.getPostList();
            List<Post> postListNew = users.getPostList();
            List<String> illegalOrphanMessages = null;
            for (Post postListOldPost : postListOld) {
                if (!postListNew.contains(postListOldPost)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Post " + postListOldPost + " since its idUser field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Users> attachedUsersListNew = new ArrayList<Users>();
            for (Users usersListNewUsersToAttach : usersListNew) {
                usersListNewUsersToAttach = em.getReference(usersListNewUsersToAttach.getClass(), usersListNewUsersToAttach.getIdUsers());
                attachedUsersListNew.add(usersListNewUsersToAttach);
            }
            usersListNew = attachedUsersListNew;
            users.setUsersList(usersListNew);
            List<Users> attachedUsersList1New = new ArrayList<Users>();
            for (Users usersList1NewUsersToAttach : usersList1New) {
                usersList1NewUsersToAttach = em.getReference(usersList1NewUsersToAttach.getClass(), usersList1NewUsersToAttach.getIdUsers());
                attachedUsersList1New.add(usersList1NewUsersToAttach);
            }
            usersList1New = attachedUsersList1New;
            users.setUsersList1(usersList1New);
            List<Hastag> attachedHastagListNew = new ArrayList<Hastag>();
            for (Hastag hastagListNewHastagToAttach : hastagListNew) {
                hastagListNewHastagToAttach = em.getReference(hastagListNewHastagToAttach.getClass(), hastagListNewHastagToAttach.getHastagName());
                attachedHastagListNew.add(hastagListNewHastagToAttach);
            }
            hastagListNew = attachedHastagListNew;
            users.setHastagList(hastagListNew);
            List<Post> attachedPostListNew = new ArrayList<Post>();
            for (Post postListNewPostToAttach : postListNew) {
                postListNewPostToAttach = em.getReference(postListNewPostToAttach.getClass(), postListNewPostToAttach.getIdPost());
                attachedPostListNew.add(postListNewPostToAttach);
            }
            postListNew = attachedPostListNew;
            users.setPostList(postListNew);
            users = em.merge(users);
            for (Users usersListOldUsers : usersListOld) {
                if (!usersListNew.contains(usersListOldUsers)) {
                    usersListOldUsers.getUsersList().remove(users);
                    usersListOldUsers = em.merge(usersListOldUsers);
                }
            }
            for (Users usersListNewUsers : usersListNew) {
                if (!usersListOld.contains(usersListNewUsers)) {
                    usersListNewUsers.getUsersList().add(users);
                    usersListNewUsers = em.merge(usersListNewUsers);
                }
            }
            for (Users usersList1OldUsers : usersList1Old) {
                if (!usersList1New.contains(usersList1OldUsers)) {
                    usersList1OldUsers.getUsersList().remove(users);
                    usersList1OldUsers = em.merge(usersList1OldUsers);
                }
            }
            for (Users usersList1NewUsers : usersList1New) {
                if (!usersList1Old.contains(usersList1NewUsers)) {
                    usersList1NewUsers.getUsersList().add(users);
                    usersList1NewUsers = em.merge(usersList1NewUsers);
                }
            }
            for (Hastag hastagListOldHastag : hastagListOld) {
                if (!hastagListNew.contains(hastagListOldHastag)) {
                    hastagListOldHastag.getUsersList().remove(users);
                    hastagListOldHastag = em.merge(hastagListOldHastag);
                }
            }
            for (Hastag hastagListNewHastag : hastagListNew) {
                if (!hastagListOld.contains(hastagListNewHastag)) {
                    hastagListNewHastag.getUsersList().add(users);
                    hastagListNewHastag = em.merge(hastagListNewHastag);
                }
            }
            for (Post postListNewPost : postListNew) {
                if (!postListOld.contains(postListNewPost)) {
                    Users oldIdUserOfPostListNewPost = postListNewPost.getIdUser();
                    postListNewPost.setIdUser(users);
                    postListNewPost = em.merge(postListNewPost);
                    if (oldIdUserOfPostListNewPost != null && !oldIdUserOfPostListNewPost.equals(users)) {
                        oldIdUserOfPostListNewPost.getPostList().remove(postListNewPost);
                        oldIdUserOfPostListNewPost = em.merge(oldIdUserOfPostListNewPost);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getIdUsers();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getIdUsers();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Post> postListOrphanCheck = users.getPostList();
            for (Post postListOrphanCheckPost : postListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Post " + postListOrphanCheckPost + " in its postList field has a non-nullable idUser field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Users> usersList = users.getUsersList();
            for (Users usersListUsers : usersList) {
                usersListUsers.getUsersList().remove(users);
                usersListUsers = em.merge(usersListUsers);
            }
            List<Users> usersList1 = users.getUsersList1();
            for (Users usersList1Users : usersList1) {
                usersList1Users.getUsersList().remove(users);
                usersList1Users = em.merge(usersList1Users);
            }
            List<Hastag> hastagList = users.getHastagList();
            for (Hastag hastagListHastag : hastagList) {
                hastagListHastag.getUsersList().remove(users);
                hastagListHastag = em.merge(hastagListHastag);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
