/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.controller;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mvc.modelo.entidades.Users;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import mvc.controlador.entidades.exceptions.NonexistentEntityException;
import mvc.controlador.entidades.exceptions.PreexistingEntityException;
import mvc.modelo.entidades.Hastag;
import mvc.modelo.entidades.Post;

/**
 *
 * @author ChRix
 */
public class HastagJpaController implements Serializable {

    public HastagJpaController() {
        this.emf = Persistence.createEntityManagerFactory("MyPostPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hastag hastag) throws PreexistingEntityException, Exception {
        if (hastag.getUsersList() == null) {
            hastag.setUsersList(new ArrayList<Users>());
        }
        if (hastag.getPostList() == null) {
            hastag.setPostList(new ArrayList<Post>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Users> attachedUsersList = new ArrayList<Users>();
            for (Users usersListUsersToAttach : hastag.getUsersList()) {
                usersListUsersToAttach = em.getReference(usersListUsersToAttach.getClass(), usersListUsersToAttach.getIdUsers());
                attachedUsersList.add(usersListUsersToAttach);
            }
            hastag.setUsersList(attachedUsersList);
            List<Post> attachedPostList = new ArrayList<Post>();
            for (Post postListPostToAttach : hastag.getPostList()) {
                postListPostToAttach = em.getReference(postListPostToAttach.getClass(), postListPostToAttach.getIdPost());
                attachedPostList.add(postListPostToAttach);
            }
            hastag.setPostList(attachedPostList);
            em.persist(hastag);
            for (Users usersListUsers : hastag.getUsersList()) {
                usersListUsers.getHastagList().add(hastag);
                usersListUsers = em.merge(usersListUsers);
            }
            for (Post postListPost : hastag.getPostList()) {
                postListPost.getHastagList().add(hastag);
                postListPost = em.merge(postListPost);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHastag(hastag.getHastagName()) != null) {
                throw new PreexistingEntityException("Hastag " + hastag + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hastag hastag) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hastag persistentHastag = em.find(Hastag.class, hastag.getHastagName());
            List<Users> usersListOld = persistentHastag.getUsersList();
            List<Users> usersListNew = hastag.getUsersList();
            List<Post> postListOld = persistentHastag.getPostList();
            List<Post> postListNew = hastag.getPostList();
            List<Users> attachedUsersListNew = new ArrayList<Users>();
            for (Users usersListNewUsersToAttach : usersListNew) {
                usersListNewUsersToAttach = em.getReference(usersListNewUsersToAttach.getClass(), usersListNewUsersToAttach.getIdUsers());
                attachedUsersListNew.add(usersListNewUsersToAttach);
            }
            usersListNew = attachedUsersListNew;
            hastag.setUsersList(usersListNew);
            List<Post> attachedPostListNew = new ArrayList<Post>();
            for (Post postListNewPostToAttach : postListNew) {
                postListNewPostToAttach = em.getReference(postListNewPostToAttach.getClass(), postListNewPostToAttach.getIdPost());
                attachedPostListNew.add(postListNewPostToAttach);
            }
            postListNew = attachedPostListNew;
            hastag.setPostList(postListNew);
            hastag = em.merge(hastag);
            for (Users usersListOldUsers : usersListOld) {
                if (!usersListNew.contains(usersListOldUsers)) {
                    usersListOldUsers.getHastagList().remove(hastag);
                    usersListOldUsers = em.merge(usersListOldUsers);
                }
            }
            for (Users usersListNewUsers : usersListNew) {
                if (!usersListOld.contains(usersListNewUsers)) {
                    usersListNewUsers.getHastagList().add(hastag);
                    usersListNewUsers = em.merge(usersListNewUsers);
                }
            }
            for (Post postListOldPost : postListOld) {
                if (!postListNew.contains(postListOldPost)) {
                    postListOldPost.getHastagList().remove(hastag);
                    postListOldPost = em.merge(postListOldPost);
                }
            }
            for (Post postListNewPost : postListNew) {
                if (!postListOld.contains(postListNewPost)) {
                    postListNewPost.getHastagList().add(hastag);
                    postListNewPost = em.merge(postListNewPost);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = hastag.getHastagName();
                if (findHastag(id) == null) {
                    throw new NonexistentEntityException("The hastag with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hastag hastag;
            try {
                hastag = em.getReference(Hastag.class, id);
                hastag.getHastagName();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hastag with id " + id + " no longer exists.", enfe);
            }
            List<Users> usersList = hastag.getUsersList();
            for (Users usersListUsers : usersList) {
                usersListUsers.getHastagList().remove(hastag);
                usersListUsers = em.merge(usersListUsers);
            }
            List<Post> postList = hastag.getPostList();
            for (Post postListPost : postList) {
                postListPost.getHastagList().remove(hastag);
                postListPost = em.merge(postListPost);
            }
            em.remove(hastag);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hastag> findHastagEntities() {
        return findHastagEntities(true, -1, -1);
    }

    public List<Hastag> findHastagEntities(int maxResults, int firstResult) {
        return findHastagEntities(false, maxResults, firstResult);
    }

    private List<Hastag> findHastagEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hastag.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hastag findHastag(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hastag.class, id);
        } finally {
            em.close();
        }
    }

    public int getHastagCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hastag> rt = cq.from(Hastag.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
