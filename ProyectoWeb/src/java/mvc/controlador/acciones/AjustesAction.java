package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.UsersJpaController;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AjustesAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        String userDescription = request.getParameter("description");

        Users usuario = (Users) session.getAttribute("datos");

        if (userDescription != null && !usuario.getUserDescription().equals(userDescription)) {
            usuario.setUserDescription(userDescription);
            UsersJpaController ujc = new UsersJpaController();
            ujc.edit(usuario);
        }
        return mapping.findForward("ok");
    }
}
