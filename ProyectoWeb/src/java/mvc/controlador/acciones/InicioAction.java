package mvc.controlador.acciones;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.entidades.Hastag;
import mvc.modelo.entidades.Post;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class InicioAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        Users usuario = (Users) session.getAttribute("datos");

        List<Post> listaPost = usuario.getPostList();
        List<Hastag> listaHastag = usuario.getHastagList();
        Comparator<Post> comparator = (s2, s1) -> s1.getIdPost().compareTo(s2.getIdPost());
        List<Post> lps = new ArrayList();

        for (Hastag h : listaHastag) {
            listaPost = h.getPostList();
            for (Post p : listaPost) {
                lps.add(p);
            }
        }
        request.setAttribute("lps", lps);
        lps.sort(comparator);
        return mapping.findForward("inicio");
    }
}
