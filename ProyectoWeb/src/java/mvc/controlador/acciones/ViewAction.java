package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.UsersJpaController;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ViewAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        UsersJpaController ujc = new UsersJpaController();
        EntityManager em = ujc.getEntityManager();
        try {
            String Query = request.getParameter("u");
            Query q = em.createNamedQuery("Users.findByUserName");
            q.setParameter("userName", Query);
            session.setAttribute("perfil", q.getSingleResult());
        } catch (Exception e) {
            return mapping.findForward("error");
        } finally {
            em.close();
        }
        return mapping.findForward("perfil");
    }
}
