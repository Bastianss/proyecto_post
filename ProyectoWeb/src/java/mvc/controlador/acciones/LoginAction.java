package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.UsersJpaController;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
       
        String usuario = request.getParameter("usuario");
        String clave = request.getParameter("clave");
        if (usuario != null) {
            UsersJpaController ujc = new UsersJpaController();
            EntityManager em = ujc.getEntityManager();
            Query q = em.createNamedQuery("Users.findByUserName");
            q.setParameter("userName", usuario);
            try {
                Users user = (Users) q.getSingleResult();
                if (usuario.equals(user.getUserName()) && clave.equals(user.getPassword())) {
                    session.setAttribute("datos", user);
                    return mapping.findForward("ok");
                }
            } catch (Exception e) {
                request.setAttribute("error", "Usuario o clave incorrectos");
                return mapping.findForward("fallo");
            } finally {
                em.close();
            }
        }
        request.setAttribute("error", "Usuario o clave incorrectos");
        return mapping.findForward("fallo");
    }
}
