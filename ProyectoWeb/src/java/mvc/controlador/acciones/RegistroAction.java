package mvc.controlador.acciones;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.UsersJpaController;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RegistroAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String email = request.getParameter("email");
        String name = request.getParameter("usuario");
        String password = request.getParameter("clave");
        String description = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = (sdf.format(new Date()));
        try {
            if ((!email.isEmpty() && email.contains("@")) && (name.length() < 15) && (!name.isEmpty()) && (!password.isEmpty() && password.length() < 12)) {
                Users nuevo = new Users(name, email, password, fecha, description);
                UsersJpaController controller = new UsersJpaController();
                controller.create(nuevo);
                HttpSession session = request.getSession();
                session.setAttribute("datos", nuevo);
                return mapping.findForward("Rok");
            } else {
                request.setAttribute("error", "Fallo en registro de usuario");
                return mapping.findForward("fallo");
            }
        } catch (Exception e) {
            request.setAttribute("error", "Fallo en registro de usuario");
            return mapping.findForward("fallo");
        }
    }
}
