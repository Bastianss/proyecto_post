package mvc.controlador.acciones;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.UsersJpaController;
import mvc.modelo.entidades.Hastag;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class HastagAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        String add = request.getParameter("a");
        String delete = request.getParameter("d");
        Hastag h = (Hastag) session.getAttribute("addHastag");

        UsersJpaController ujc = new UsersJpaController();
        Users usuario = (Users) session.getAttribute("datos");
        List<Hastag> hastags = (List<Hastag>) usuario.getHastagList();

        if (add != null) {
            if (!hastags.contains(h)) {
                hastags.add(h);
                usuario.setHastagList(hastags);

                ujc.edit(usuario);
            }
        }
        if (delete != null) {
            try {
                for (Hastag hs : hastags) {
                    if (hs.getHastagName().equals(delete)) {
                        hastags.remove(hs);
                        usuario.setHastagList(hastags);
                        ujc.edit(usuario);
                    }
                }
            } catch (Exception e) {
            }
        }

        return mapping.findForward("ok");
    }
}
