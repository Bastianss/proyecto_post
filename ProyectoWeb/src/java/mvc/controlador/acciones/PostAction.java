package mvc.controlador.acciones;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.HastagJpaController;
import mvc.modelo.controller.PostJpaController;
import mvc.modelo.entidades.Hastag;
import mvc.modelo.entidades.Post;
import mvc.modelo.entidades.Users;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class PostAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String post = request.getParameter("post");
        String hastags = request.getParameter("hastags");
        String deleteId = request.getParameter("id");

        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        Users usuario = (Users) session.getAttribute("datos");

        if (deleteId != null) {
            PostJpaController pjc = new PostJpaController();
            List<Post> listPost = usuario.getPostList();
            for (Post p : listPost) {
                if (p.getIdPost().equals(Integer.parseInt(deleteId))) {
                    pjc.destroy(Integer.parseInt(deleteId));
                    listPost.remove(p);
                    usuario.setPostList(listPost);
                    return mapping.findForward("delete");
                }
            }
        }
        try {
            if (!hastags.isEmpty() && hastags != null) {
                HastagJpaController hjc = new HastagJpaController();
                if (hjc.findHastag(hastags) == null) {
                    hjc.create(new Hastag(hastags));
                }

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String fecha = (sdf.format(new Date()));
                Post p = new Post(post, fecha);
                p.setIdUser(usuario);
                List<Hastag> hastagList = new ArrayList();
                hastagList.add(new Hastag(hastags));
                p.setHastagList(hastagList);
                PostJpaController pjc = new PostJpaController();
                pjc.create(p);
                usuario.getPostList().add(0, p);
            }
        } catch (Exception e) {
        }
        return mapping.findForward("post");
    }
}
