package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mvc.modelo.controller.HastagJpaController;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class SearchAction extends org.apache.struts.action.Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("datos") == null) {
            return mapping.findForward("fallo");
        }
        try {
            HastagJpaController hjc = new HastagJpaController();
            EntityManager em = hjc.getEntityManager();

            String Query = request.getParameter("q");
            Query q = em.createNamedQuery("Hastag.findByHastagName");
            q.setParameter("hastagName", Query);
            session.setAttribute("resultado", q.getSingleResult());
        } catch (Exception e) {
            request.setAttribute("error", true);
        }
        return mapping.findForward("buscar");
    }
}
